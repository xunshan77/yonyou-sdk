<?php
try {
    // 1. 手动加载入口文件
    include "../src/include.php";

    // 2. 准备公众号配置参数
    $config = [
        "from_account" => "your from_account",
        "app_key" => "your app_key",
        "app_secret" => "your app_secret"
    ];

    // 3. 创建接口实例
    //$platform = new \Yonyou\U8\Platform($config);
    //$platform = \Yonyou::U8Platform($config);
    $platform = \Yonyou\U8\Platform::instance($config);

    // 4. 组装参数，可以参考官方商户文档
    $urlParams = [
        'to_account' => "your to_account"
    ];
    $result = $platform->orderstatus($urlParams);

    var_export($result);

} catch (Exception $e) {
    // 出错啦，处理下吧
    echo $e->getMessage() . PHP_EOL;
}
