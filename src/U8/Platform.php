<?php

namespace Yonyou\U8;

use Yonyou\Contracts\BaseU8;
use Yonyou\Exceptions\InvalidArgumentException;
use Yonyou\Exceptions\InvalidResponseException;
use Yonyou\Exceptions\LocalCacheException;

/**
 * 平台类
 * Class Platform
 *
 * @package Yonyou\U8
 */
class Platform extends BaseU8
{
    /**
     * 获取交易号
     *
     * @return array
     * @throws InvalidResponseException
     * @throws LocalCacheException
     */
    public function tradeid()
    {
        $url = "https://api.yonyouup.com/system/tradeid?from_account=" . $this->config->get('from_account')
            . "&app_key=" . $this->config->get('app_key')
            . "&token=TOKEN";
        $this->registerApi($url, __FUNCTION__, func_get_args());
        return $this->httpGetForJson($url);
    }

    /**
     * 根据用户，获取其应用购买状态
     *
     * @param array $urlParams
     * @return array
     * @throws InvalidResponseException
     * @throws LocalCacheException
     */
    public function orderstatus($urlParams = array())
    {
        if (!isset($urlParams['to_account']) || empty($urlParams['to_account'])) {
            throw new InvalidArgumentException('缺少Url参数 [to_account]');
        }
        $url = "https://api.yonyouup.com/api/orderstatus/get?from_account=" . $this->config->get('from_account')
            . "&to_account=" . $urlParams['to_account']
            . "&app_key=" . $this->config->get('app_key')
            . "&token=TOKEN";
        $this->registerApi($url, __FUNCTION__, func_get_args());
        return $this->httpGetForJson($url);
    }
}
