<?php
spl_autoload_register(function ($classname) {
    $pathname = __DIR__ . DIRECTORY_SEPARATOR;
    $filename = str_replace('\\', DIRECTORY_SEPARATOR, $classname) . '.php';
    if (file_exists($pathname . $filename)) {
        foreach (['U8'] as $prefix) {
            if (stripos($classname, $prefix) === 0) {
                echo $pathname . $filename . PHP_EOL;
                include $pathname . $filename;
                return true;
            }
        }
    }
    return false;
});
