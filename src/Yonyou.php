<?php

use Yonyou\Contracts\DataArray;
use Yonyou\Exceptions\InvalidInstanceException;
use Yonyou\U8\Platform;

/**
 * 加载缓存器
 *
 * ----- U8 -----
 * @method Platform U8Platform($options = []) static U8平台相关
 */
class Yonyou
{
    /**
     * 定义当前版本
     * @var string
     */
    const VERSION = '1.0.1';

    /**
     * 静态配置
     * @var DataArray
     */
    private static $config;

    /**
     * 设置及获取参数
     * @param array $option
     * @return array
     */
    public static function config($option = null)
    {
        if (is_array($option)) {
            self::$config = new DataArray($option);
        }
        if (self::$config instanceof DataArray) {
            return self::$config->get();
        }
        return [];
    }

    /**
     * 静态魔术加载方法
     * @param string $name 静态类名
     * @param array $arguments 参数集合
     * @return mixed
     * @throws InvalidInstanceException
     */
    public static function __callStatic($name, $arguments)
    {
        if (substr($name, 0, 2) === 'U8') {
            $class = 'Yonyou\\U8\\' . substr($name, 2);
        }
        if (!empty($class) && class_exists($class)) {
            $option = array_shift($arguments);
            $config = is_array($option) ? $option : self::$config->get();
            return new $class($config);
        }
        throw new InvalidInstanceException("class {$name} not found");
    }

}
