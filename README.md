yonyou-sdk for PHP
----

* 运行最底要求 PHP 版本 5.4 , 建议在 PHP7 上运行以获取最佳性能；
* 接口需要缓存数据在本地，因此对目录需要有写权限；
* 我们鼓励大家使用 composer 来管理您的第三方库，方便后期更新操作；

代码仓库
----
yonyou-sdk 为开源项目，允许把它用于任何地方，不受任何约束，欢迎 fork 项目。

* Gitee 托管地址：https://gitee.com/xunshan77/yonyou-sdk

文件说明（后缀会根据官方文档增加文件）
----

| 文件名          | 类型                 | 描述      | 加载                    |
|--------------|--------------------|---------|-----------------------|
| Platform.php | Youyou\U8\Platform | U8+平台相关 | \Yonyou::U8Platform() |


安装使用
----
1.1 通过 Composer 来管理安装

```shell
# 首次安装 线上版本（稳定）
composer require 77xunshan/yonyou-sdk

# 首次安装 开发版本（开发）
composer require 77xunshan/yonyou-sdk dev-master

# 更新
composer update 77xunshan/yonyou-sdk
```

1.2 如果不使用 Composer， 可以下载 yonyou-sdk 并解压到项目中

```php
# 在项目中加载初始化文件
include "您的目录/yonyou-sdk/src/include.php";
```

2.1 接口实例所需参数

```php
$config = [
    'from_account'   => 'test',
    'app_key'        => 'your key',
    'app_secret'     => 'your secret',
    // 缓存目录配置（可选，需拥有读写权限）
    'cache_path'     => '',
];
```

3.1 实例指定接口

```php
try {

    // 实例对应的接口对象
    $user = new \Yonyou\U8\Platform($config);
    
    // 调用接口对象方法
    $list = $user->tradeid();
    
    // 处理返回的结果
    echo '<pre>';
    var_export($list);
    
} catch (Exception $e) {

    // 出错啦，处理下吧
    echo $e->getMessage() . PHP_EOL;
    
}
```

根据用户，获取其应用购买状态
---

```php
  // 创建接口实例
  $platform = new \Yonyou\U8\Platform($config);
  
  // 组装参数，可以参考官方文档
  $urlParams = [
      'to_account'       => 'your to_account'
  ];
    
try {
    // 生成预支付码
    $result = $platform->orderstatus($urlParams);
    
} catch (Exception $e) {

    // 出错啦，处理下吧
    echo $e->getMessage() . PHP_EOL;
    
}
```

* 更多功能请阅读测试代码或SDK封装源码

开源协议
----

* yonyou-sdk 基于`MIT`协议发布，任何人可以用在任何地方，不受约束
* yonyou-sdk 部分代码来自互联网，若有异议，可以联系作者进行删除
